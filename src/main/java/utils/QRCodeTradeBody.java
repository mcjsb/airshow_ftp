package utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 *  航展二维码交易记录 128 bytes
 * * int = 4 bytes
 * * 为了简便，小于4 bytes 的也用int表示
 *
 * @author likai
 * @date 2018/09/05
 */
public class QRCodeTradeBody {
    /**
     * 交易类型不在这里解析,故128 - 1 bytes
     */
    private final int SIZE = 127;

    /**
     * 交易类型,1 byte
     */
    private int tradeType = 0x3d;

    /**
     * 交易日期, 4 bytes
     */
    private LocalDate gatherDate;

    /**
     * 交易时间, 3 bytes
     */
    private LocalTime gatherTime;

    /**
     * 终端交易序号（POS), 4 bytes
     */
    private int terminalTradeSequence;

    /**
     * 上车人数, 1 bytes
     */
    private int passengerNumber;

    /**
     * 交易性质 1 byte
     */
    private int tradeProperty;

    /**
     * 车号 5 bytes
     */
    private String carNumber;

    /**
     * 线路号 3 bytes
     */
    private String roadNumber;

    /**
     * 二维码ID信息 96 bytes
     */
    private String QRCodeInformation;

    public String getQRCodeInformationId() {
        return QRCodeInformationId;
    }

    public void setQRCodeInformationId(String QRCodeInformationId) {
        this.QRCodeInformationId = QRCodeInformationId;
    }

    private String QRCodeInformationId;

    /**
     * 纬度 4 byte
     */
    private int latitude;

    /**
     * 经度 4 byte
     */
    private int longitude;

    /**
     * CRC16校验, 2 byte
     */
    private int CRC;

    public InputStream getFile() {
        return file;
    }

    public void setFile(InputStream file) {
        this.file = file;
    }

    private InputStream file;


    public void parse() {
        byte[] bytes = new byte[SIZE];

        try {
            file.read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * 交易类型,1 byte，不解析
         */

        /**
         * 每个数据的开始坐标
         */
        int index = 0;

        /**
         * 交易日期, 4 bytes
         */
        byte[] gatherDateArray = Arrays.copyOfRange(bytes, index, index +4);
        String gatherDateStr = BCD.BCDtoString(gatherDateArray);
        this.gatherDate = LocalDate.parse(gatherDateStr, DateTimeFormatter.BASIC_ISO_DATE);
        index = index + 4;

        /**
         * 采集时间, 3 bytes
         */
        byte[] gatherTimeArray = Arrays.copyOfRange(bytes, index, index + 3 );
        String gatherTimeStr = BCD.BCDtoString(gatherTimeArray);
        gatherTime = LocalTime.parse(gatherTimeStr, DateTimeFormatter.ofPattern("HHmmss"));
        index = index + 3;

        /**
         * 终端交易序号（POS), 4 bytes
         */
        byte[] terminalTradeSequenceArray = Arrays.copyOfRange(bytes, index, index + 4);
        this.terminalTradeSequence = java.nio.ByteBuffer.wrap(terminalTradeSequenceArray).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
        index = index + 4;

        /**
         * 上车人数, 1 bytes
         */
        Byte passengerNumber = bytes[index];
        this.passengerNumber = passengerNumber.intValue();
        index = index + 1;

        /**
         * 交易性质 1 byte
         */
        Byte tradeProperty = bytes[index];
        this.tradeProperty = tradeProperty.intValue();
        index = index + 1;

        /**
         * 车号 5 bytes
         */
        byte[] carNumberArray = Arrays.copyOfRange(bytes, index, index + 5);
        try {
            this.carNumber = new String(carNumberArray, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        index += 5;

        /**
         * 线路号 3 bytes
         */
        byte[] roadNumberArray = Arrays.copyOfRange(bytes, index, index + 3);
        index += 3;

        /**
         * 二维码ID信息 96 bytes
         */
        byte[] QRCodeInfomationBytes = Arrays.copyOfRange(bytes, index, index + 96);
        int length = QRCodeInfomationBytes[0];
        byte[] contentBytes = Arrays.copyOfRange(QRCodeInfomationBytes, 1, 1 + length);

        try {
            this.QRCodeInformation = new String(contentBytes, "UTF-8");
            this.QRCodeInformation = this.QRCodeInformation.trim();
            int startIndex = this.QRCodeInformation.lastIndexOf('=') + 1;
            this.QRCodeInformationId = this.QRCodeInformation.substring(startIndex);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        index += 96;

        /**
         * 纬度 4 byte
         */
        byte[] attitudeArray = Arrays.copyOfRange(bytes, index, index + 4);
        index += 4;

        /**
         * 经度 4 byte
         */
        byte[] longitudeArray = Arrays.copyOfRange(bytes, index, index + 4);
        index += 4;

        /**
         * CRC16校验, 2 byte
         */
        byte[] CRCArray = Arrays.copyOfRange(bytes, index, index + 2);
        this.CRC = java.nio.ByteBuffer.wrap(CRCArray).order(java.nio.ByteOrder.LITTLE_ENDIAN).getChar();
    }

    public int getSIZE() {
        return SIZE;
    }

    public int getTradeType() {
        return tradeType;
    }

    public void setTradeType(int tradeType) {
        this.tradeType = tradeType;
    }

    public LocalDate getGatherDate() {
        return gatherDate;
    }

    public void setGatherDate(LocalDate gatherDate) {
        this.gatherDate = gatherDate;
    }

    public LocalTime getGatherTime() {
        return gatherTime;
    }

    public void setGatherTime(LocalTime gatherTime) {
        this.gatherTime = gatherTime;
    }

    public int getTerminalTradeSequence() {
        return terminalTradeSequence;
    }

    public void setTerminalTradeSequence(int terminalTradeSequence) {
        this.terminalTradeSequence = terminalTradeSequence;
    }

    public int getPassengerNumber() {
        return passengerNumber;
    }

    public void setPassengerNumber(int passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public int getTradeProperty() {
        return tradeProperty;
    }

    public void setTradeProperty(int tradeProperty) {
        this.tradeProperty = tradeProperty;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getQRCodeInformation() {
        return QRCodeInformation;
    }

    public void setQRCodeInformation(String QRCodeInformation) {
        this.QRCodeInformation = QRCodeInformation;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public int getCRC() {
        return CRC;
    }

    public void setCRC(int CRC) {
        this.CRC = CRC;
    }

    public QRCodeTradeBody(InputStream file) {
        this.file = file;
        this.parse();
    }
}
