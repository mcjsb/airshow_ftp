package utils;

import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

/**
 * BCD压缩算法测试类
 * @author likai
 * @date 2018/09/06
 */
public class BCDTest {

    @Test
    public void testAll(){
        /**
         * success test
         */
        test(20170328, "00100000000101110000001100101000");
        test(20180906, "00100000000110000000100100000110");

        /**
         * fail test
         */
        test(20180906, "00100000000110000100100100000110");

    }

    public void test(long expectValue, String expect){
        byte[] bval = new BigInteger(expect, 2).toByteArray();
        String actual = BCD.BCDtoString(bval);
        long actualValue = Long.valueOf(actual);
        Assert.assertEquals(expectValue,actualValue);
    }

}
