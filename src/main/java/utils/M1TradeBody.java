package utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

/**
 * 航展M1卡交易记录 128 bytes
 * * int = 4 bytes
 * * 为了简便，小于4 bytes 的也用int表示
 *
 * @author likai
 * @date 2018/09/05
 */
public class M1TradeBody {
    /**
     * 交易类型不在这里解析,故128 - 1 bytes
     */
    private final int SIZE = 127;

    /**
     * 交易类型,1 byte
     */
    private int tradeType = 0x3c;

    /**
     * 应用卡号, 7 bytes
     */
    private String applicationCardNumber;

    /**
     * 交易日期, 4 bytes
     */
    private LocalDate gatherDate;

    /**
     * 采集时间, 3 bytes
     */
    private LocalTime gatherTime;

    /**
     * 终端交易序号（POS), 4 bytes
     */
    private int terminalTradeSequence;

    /**
     * 物理卡号（POS), 4 bytes
     */
    private int physicalCardNumber;

    /**
     * 交易性质 1 byte
     */
    private int tradeProperty;

    /**
     * 车号 5 bytes
     */
    private String carNumber;

    /**
     * 纬度 4 byte
     */
    private int latitude;

    /**
     * 经度 4 byte
     */
    private int longitude;


    /**
     * 预留, 89 byte
     */
    private byte[] reservations = new byte[89];

    /**
     * CRC16校验, 2 byte
     */
    private int CRC;

    private InputStream file;


    public InputStream getFile() {
        return file;
    }

    public void setFile(InputStream file) {
        this.file = file;
    }

    public void parse() {
        byte[] bytes = new byte[SIZE];
        try {
            file.read(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        /**
         * 交易类型,1 byte， 不在这解析
         */


        /**
         * 每个数据的开始坐标
         */
        int index = 0;

        /**
         * 应用卡号, 7 bytes
         */
        byte[] applicationCardNumberArray = Arrays.copyOfRange(bytes, index, index + 7);
        try {
            this.applicationCardNumber = new String(applicationCardNumberArray, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        index += 7;

        /**
         * 交易日期, 4 bytes
         */
        byte[] gatherDateArray = Arrays.copyOfRange(bytes, index, index + 4);
        String gatherDateStr = BCD.BCDtoString(gatherDateArray);
        this.gatherDate = LocalDate.parse(gatherDateStr, DateTimeFormatter.BASIC_ISO_DATE);
        index += 4;

        /**
         * 采集时间, 3 bytes
         */
        byte[] gatherTimeArray = Arrays.copyOfRange(bytes, index, index + 3);
        String gatherTimeStr = BCD.BCDtoString(gatherTimeArray);
        gatherTime = LocalTime.parse(gatherTimeStr, DateTimeFormatter.ofPattern("HHmmss"));
        index += 3;

        /**
         * 终端交易序号（POS), 4 bytes
         */
        byte[] terminalTradeSequenceArray = Arrays.copyOfRange(bytes, index, index + 4);
        this.terminalTradeSequence = java.nio.ByteBuffer.wrap(terminalTradeSequenceArray).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
        index += 4;

        /**
         * 物理卡号（POS), 4 bytes
         */
        byte[] physicalCardNumberArray = Arrays.copyOfRange(bytes, index, index + 4);
        this.physicalCardNumber = java.nio.ByteBuffer.wrap(physicalCardNumberArray).order(java.nio.ByteOrder.LITTLE_ENDIAN).getInt();
        index += 4;

        /**
         * 交易性质 1 byte
         */
        Byte tradeProperty = bytes[index];
        this.tradeProperty = tradeProperty.intValue();
        index += 1;

        /**
         * 车号 5 bytes
         */
        byte[] carNumberArray = Arrays.copyOfRange(bytes, index, index + 5);
        try {
            this.carNumber = new String(carNumberArray, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        index += 5;

        /**
         * 纬度 4 byte
         */
        byte[] attitudeArray = Arrays.copyOfRange(bytes, index, index + 4);
        index += 4;


        /**
         * 经度 4 byte
         */
        byte[] longitudeArray = Arrays.copyOfRange(bytes, index, index + 4);
        index += 4;

        /**
         * 预留, 89 byte
         */
        this.reservations = Arrays.copyOfRange(bytes, index, index + 89);
        index += 89;

        /**
         * CRC16校验, 2 byte
         */
        byte[] CRCArray = Arrays.copyOfRange(bytes, index, index + 2);
        this.CRC = java.nio.ByteBuffer.wrap(CRCArray).order(java.nio.ByteOrder.LITTLE_ENDIAN).getChar();
        index += 2;
    }

    public M1TradeBody(InputStream file) {
        this.file = file;
        this.parse();
    }

    public int getSIZE() {
        return SIZE;
    }

    public int getTradeType() {
        return tradeType;
    }

    public void setTradeType(int tradeType) {
        this.tradeType = tradeType;
    }

    public String getApplicationCardNumber() {
        return applicationCardNumber;
    }

    public void setApplicationCardNumber(String applicationCardNumber) {
        this.applicationCardNumber = applicationCardNumber;
    }

    public LocalDate getGatherDate() {
        return gatherDate;
    }

    public void setGatherDate(LocalDate gatherDate) {
        this.gatherDate = gatherDate;
    }

    public LocalTime getGatherTime() {
        return gatherTime;
    }

    public void setGatherTime(LocalTime gatherTime) {
        this.gatherTime = gatherTime;
    }

    public int getTerminalTradeSequence() {
        return terminalTradeSequence;
    }

    public void setTerminalTradeSequence(int terminalTradeSequence) {
        this.terminalTradeSequence = terminalTradeSequence;
    }

    public int getPhysicalCardNumber() {
        return physicalCardNumber;
    }

    public void setPhysicalCardNumber(int physicalCardNumber) {
        this.physicalCardNumber = physicalCardNumber;
    }

    public int getTradeProperty() {
        return tradeProperty;
    }

    public void setTradeProperty(int tradeProperty) {
        this.tradeProperty = tradeProperty;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public int getLatitude() {
        return latitude;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public int getLongitude() {
        return longitude;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public byte[] getReservations() {
        return reservations;
    }

    public void setReservations(byte[] reservations) {
        this.reservations = reservations;
    }

    public int getCRC() {
        return CRC;
    }

    public void setCRC(int CRC) {
        this.CRC = CRC;
    }
}
