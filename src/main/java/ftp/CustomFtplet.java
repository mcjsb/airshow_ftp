package ftp;

import org.apache.ftpserver.ftplet.DefaultFtplet;
import org.apache.ftpserver.ftplet.FtpRequest;
import org.apache.ftpserver.ftplet.FtpSession;
import org.apache.ftpserver.ftplet.FtpletResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ParseTradeFile;

/**
 * 重载文件上传完成监听
 * @author likai
 * @date 2018/9/17
 */
public class CustomFtplet extends DefaultFtplet {
    Logger logger = LoggerFactory.getLogger(CustomFtplet.class);

    @Override
    public FtpletResult onUploadEnd(FtpSession session, FtpRequest request){
        ParseTradeFile parseTradeFile = new ParseTradeFile();
        logger.info("上传文件完成");
        String fileName = request.getArgument();
        String realPath = FtpConfig.HOME_DIR + fileName;

        logger.info("上传的文件为: " + realPath);

        /**
         * 判断是否为.312结尾的的文件
         */
        String recordFileSuffix = ".312";
        if(fileName.endsWith(recordFileSuffix)){
            parseTradeFile.parseFile(fileName, realPath, "batch");
        }else{
            logger.info("不是合法的记录文件: " + fileName);
        }
        return FtpletResult.DEFAULT;
    }
}
