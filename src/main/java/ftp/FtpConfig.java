package ftp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import utils.ParseTradeFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * FTP配置类
 * @author likai
 * @date 2018/9/17
 */
public class FtpConfig {
    private static Logger logger = LoggerFactory.getLogger(FtpConfig.class);
    private static Properties props;
    static{
        loadProps();
    }

    synchronized static private void loadProps(){
        logger.info("开始加载properties文件内容.......");
        props = new Properties();
        InputStream in = null;
        try {
//　　　　　　　<!--第一种，通过类加载器进行获取properties文件流-->
                    in = FtpConfig.class.getClassLoader().getResourceAsStream("ftp.properties");
//　　　　　　  <!--第二种，通过类进行获取properties文件流-->
                    //in = PropertyUtil.class.getResourceAsStream("/jdbc.properties");
                    props.load(in);
        } catch (FileNotFoundException e) {
            logger.error("jdbc.properties文件未找到");
        } catch (IOException e) {
            logger.error("出现IOException");
        } finally {
            try {
                if(null != in) {
                    in.close();
                }
            } catch (IOException e) {
                logger.error("jdbc.properties文件流关闭出现异常");
            }
        }
        logger.info("加载properties文件内容完成...........");
        logger.info("properties文件内容：" + props);
    }

    public static String getProperty(String key){
        if(null == props) {
            loadProps();
        }
        return props.getProperty(key);
    }
    /**
     * ftp目录
     */
    public static final String HOME_DIR = props.getProperty("HOME_DIR");
    /**
     * 用户名
     */
    public static final String FTP_NAME = props.getProperty("FTP_NAME");
    /**
     * 密码
     */
    public static final String PASSWORD = props.getProperty("PASSWORD");
    /**
     * 连接端口
     */
    public static final int PORT = Integer.parseInt(props.getProperty("PORT"));
    /**
     * 被动数据端口
     */
    public static final String PASSIVE_PORT = props.getProperty("PASSIVE_PORT");
    /**
     * 服务器用
     */
    public static final String EXTERNAL_ADDRESS = props.getProperty("EXTERNAL_ADDRESS");
    /**
     * 本地调试用
     */
//    public static final String EXTERNAL_ADDRESS = props.getProperty("HOME_DIR");

    /**
     * 通用二维码id
     */
    public static final String SPECIAL_ID = props.getProperty("SPECIAL_ID");
    public static final List<String> SPECIAL_ID_LIST = Arrays.asList(SPECIAL_ID.split(","));
}
